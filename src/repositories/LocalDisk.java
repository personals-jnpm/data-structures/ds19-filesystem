package repositories;

import models.Archive;
import models.TypeArchive;

import java.util.TreeMap;

public class LocalDisk {

    private static final String ROOT_FOLDER_NAME = "root";
    private static final String FILES_FOLDER_NAME = "files";

    private TreeMap<String, Archive> content;
    private Archive rootFolder;
    private Archive filesFolder;

    private Archive currentFolder;
    private String currentPath;

    public LocalDisk() {
        content = new TreeMap<>();
        initRootFolder();
        initFilesFolder();
        initLocalDisk();
        currentFolder = filesFolder;
        currentPath = "/" + FILES_FOLDER_NAME;
        initContentRootFolder();
    }

    private void initLocalDisk() {
        content.put("/" + ROOT_FOLDER_NAME, rootFolder);
        content.put("/" + FILES_FOLDER_NAME, filesFolder);
    }

    private void initRootFolder() {
        rootFolder = new Archive(ROOT_FOLDER_NAME, TypeArchive.Folder);
    }

    private void initFilesFolder() {
        filesFolder = new Archive(FILES_FOLDER_NAME, TypeArchive.Folder);
    }

    public TreeMap<String, Archive> getContent() {
        return content;
    }

    public void setContent(TreeMap<String, Archive> content) {
        this.content = content;
    }

    public Archive getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(Archive rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Archive getFilesFolder() {
        return filesFolder;
    }

    public void setFilesFolder(Archive filesFolder) {
        this.filesFolder = filesFolder;
    }

    public Archive getCurrentFolder() {
        return currentFolder;
    }

    public void setCurrentFolder(Archive currentFolder) {
        this.currentFolder = currentFolder;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String currentPath) {
        this.currentPath = currentPath;
    }

    public void addArchive(String fileName, Archive archive) {
        try {
            currentFolder.addArchive(currentPath + "/" + fileName, archive);
        } catch (Exception e) {
            System.out.println(" ¡Error al agregar archivo!");
        }
    }

    public void accessFolderByPath(String fileName) {
        try {
            Archive archive = currentFolder.findArchive(currentPath + "/" + fileName);
            if (archive != null && archive.getFiles() != null) {
                currentFolder = currentFolder.findArchive(currentPath + "/" + fileName);
                currentPath = currentPath + "/" + fileName;
            } else {
                System.out.println(" ¡Error al acceder a la carpeta!");
            }
        } catch (Exception e) {
            System.out.println(" ¡La ruta ingresada no es válida!");
            getMainPath();
        }
    }

    public void getMainPath() {
        currentFolder = filesFolder;
    }

    public void printCurrentFolder() {
        System.out.println("\nContenido de la carpeta actual " + currentFolder.getName() + " [");
        for (String path : currentFolder.getFiles().keySet()) {
            System.out.println("\t" + currentFolder.getFiles().get(path));
        }
        System.out.println("]");
    }

    public void printDiskContent() {
        System.out.println("\nContenido del disco local [");
        for (String path : content.keySet()) {
            System.out.println("\t" + content.get(path));
            printSubFolders(content.get(path));
            System.out.println();
        }
        System.out.print("]\n");
    }

    private void printSubFolders(Archive archive) {
        if (archive.getFiles() != null) {
            for (String path : archive.getFiles().keySet()) {
                System.out.println("\t \t" + archive.getFiles().get(path));
                printSubFolders(archive.getFiles().get(path));
            }
        }
    }

    private void initContentRootFolder() {
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/dev", new Archive("dev", TypeArchive.Folder));
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/run", new Archive("run", TypeArchive.Folder));
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/etc", new Archive("etc", TypeArchive.Folder));
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/var", new Archive("var", TypeArchive.Folder));
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/bin", new Archive("bin", TypeArchive.Folder));
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/home", new Archive("home", TypeArchive.Folder));
        rootFolder.addArchive("/" + ROOT_FOLDER_NAME + "/usr", new Archive("usr", TypeArchive.Folder));

        rootFolder.findArchive("/" + ROOT_FOLDER_NAME + "/home").addArchive("/" + ROOT_FOLDER_NAME + "/home/Documents",
                new Archive("Documents", TypeArchive.Folder));
        rootFolder.findArchive("/" + ROOT_FOLDER_NAME + "/home").addArchive("/" + ROOT_FOLDER_NAME + "/home/Downloads",
                new Archive("Downloads", TypeArchive.Folder));
    }

}
