package models;

import java.util.TreeMap;

public class Archive {

    private String name;
    private TypeArchive typeArchive;
    private TreeMap<String, Archive> files;

    public Archive() {
    }

    public Archive(String name, TypeArchive typeArchive) {
        this.name = name;
        this.typeArchive = typeArchive;
        this.files = typeArchive == TypeArchive.Folder ? new TreeMap<>() : null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeArchive getTypeArchive() {
        return typeArchive;
    }

    public void setTypeArchive(TypeArchive typeArchive) {
        this.typeArchive = typeArchive;
    }

    public TreeMap<String, Archive> getFiles() {
        return files;
    }

    public Archive findArchive(String path) {
        return files.get(path);
    }

    public void addArchive(String path, Archive archive) {
        files.put(path, archive);
    }

    @Override
    public String toString() {
        return "Archivo {" +
                " nombre: " + name +
                ", tipo de archivo: " + typeArchive + " " +
                "}";
    }
}
