import controllers.LocalDiskController;
import utiliy.Keyboard;

public class Main {

    public static LocalDiskController localDiskController = new LocalDiskController();
    public static Keyboard keyboard = new Keyboard();

    public static void main(String[] args) {
        Main main = new Main();
        main.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> main.accessFolder();
                case 2 -> main.addArchive();
                case 3 -> main.printCurrentFolder();
                case 4 -> main.printDiskContent();
                case 5 -> main.mainPath();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔═══════════════════════════════════════════════════════╗");
        System.out.println("╠------------------Sistema de archivos------------------╣");
        System.out.println("║═══════════════════════════════════════════════════════║");
        System.out.println("║   1. Acceder a carpeta                                ║");
        System.out.println("║   2. Crear archivo                                    ║");
        System.out.println("║   3. Imprimir contenido de la carpeta actual          ║");
        System.out.println("║   4. Imprimir todos los archivos del disco            ║");
        System.out.println("║   5. Regresar a la ruta raíz                          ║");
        System.out.println("║═══════════════════════════════════════════════════════║");
        System.out.println("║   0. Salir                                            ║");
        System.out.println("╚═══════════════════════════════════════════════════════╝");
    }

    public void accessFolder() {
        System.out.println(" Acceder a carpeta: ");
        localDiskController.accessFolderByPath(keyboard.readLineDefault("  Ingrese el nombre de la carpeta: "));
    }

    public void addArchive() {
        System.out.println(" Agregar archivo: ");
        String fileName = keyboard.readLineDefault("  Ingrese el nombre del archivo: ");
        String typeArchive = keyboard.readLineDefault("  Ingrese el tipo de archivo (Carpeta, Archivo): ");
        localDiskController.addArchive(fileName, typeArchive);
    }

    public void printCurrentFolder() {
        localDiskController.printCurrentFolder();
    }

    public void printDiskContent() {
        localDiskController.printDiskContent();
    }

    public void mainPath() {
        localDiskController.getMainPath();
    }

}
