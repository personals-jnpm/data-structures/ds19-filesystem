package controllers;

import models.Archive;
import models.TypeArchive;
import repositories.LocalDisk;

public class LocalDiskController {

    private LocalDisk localDisk;

    public LocalDiskController() {
        localDisk = new LocalDisk();
    }

    public LocalDisk getLocalDisk() {
        return localDisk;
    }

    public void setLocalDisk(LocalDisk localDisk) {
        this.localDisk = localDisk;
    }

    public void addArchive(String fileName, String typeArchive ){
        Archive archive = new Archive(fileName, getTypeArchive(typeArchive));
        localDisk.addArchive(fileName, archive);
    }

    public void accessFolderByPath(String fileName){
        localDisk.accessFolderByPath(fileName);
    }

    public void getMainPath(){
        localDisk.getMainPath();
    }

    public void printCurrentFolder(){
        localDisk.printCurrentFolder();
    }

    public void printDiskContent(){
        localDisk.printDiskContent();
    }

    private TypeArchive getTypeArchive(String option)  {
        switch (option) {
            case "Archivo", "archivo" -> {
                return TypeArchive.File;
            }
            case "Carpeta", "carpeta" -> {
                return TypeArchive.Folder;
            }
            default -> {
                return TypeArchive.SinDefinir;
            }
        }
    }

}
